
// Import libraries for testing  (npm install <library> --save-dev) save-dev modifier 
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

// Inicialize 
chai.use(chaihttp);

//should function used for expected values (OK/KO assertions) 
var should = chai.should(
    //Here are the assertions
);

describe('First test',
    //Main handler function
    function () {
        //describe what the test does, implement the test function
        it('Test that DuckDuckgo works', function (done) {
            //Method chainging --> chai.request(<value>).get(<value>).end(function {<...>}) //Idented for better reading:
            chai.request('http://www.duckduckgo.com')
            //Commented test for incorrect asertion
            //chai.request('https://developer.mozilla.org/en-US/adsfasdfas')
                .get('/')
                .end(
                    function(err, res) {
                        //Here will be the assertions
                        //console.log(res);
                        console.log("Request has finished");
                        console.log(err);
                        res.should.have.status(200);
                        done();
                    }
                )
            }

        )
    }
)

/*describe('Test APITechU Hello',
    function () {
        it('Test that apitechu hello responds accurately', function (done) {
            chai.request('http://localhost:3000')
                .get('/apitechu/v1/hello')
                .end(
                    function(err, res) {
                        console.log("Request has finished");
                        res.should.have.status(200);
                        res.body.msg.should.be.eql("Welcome to APITechU!");
                        done();
                    }
                )
            }

        )
    }
)*/

describe('Test APITechU Users',
    function () {
        it('Test that apitechu hello responds accurately', function (done) {
            chai.request('http://localhost:3000')
                .get('/apitechu/v1/hello')
                .end(
                    function(err, res) {
                        console.log("Request has finished");
                        res.should.have.status(200);
                        res.body.msg.should.be.eql("Welcome to APITechU!");
                        done();
                    }
                )
            }
        )
        it('Test that apitechu users reponses an accurate users list', function (done) {
            chai.request('http://localhost:3000')
                .get('/apitechu/v1/users')
                .end(
                    function(err, res) {
                        console.log("Request has finished");

                        res.should.have.status(200);
                        res.body.users.should.be.a("array");
                        for (user of res.body.users){
                            user.should.have.property('email');
                            user.should.have.property('first_name');
                        }
                        
                        //res.body.msg.should.be.eql("Welcome to APITechU!");
                        done();
                    }
                )
            }
        )
    }
)