//cargamos libreria acceso io
const fs = require('fs');

//creo una funcion que me escriba en disco una lista de usuarios
function writeUserDataToFile(data){
  console.log("writeUserDataToFile");

  //transformo los datos a un formato que luego se pueda leer
  var jsonUserData = JSON.stringify(data);
  //guardo en un nuevo fichero la nueva lista con el nuevo usuario guardado
  fs.writeFile("./users.json", jsonUserData,"utf8",
    function(err){
      if(err){
        console.log(err);
      } else {
        console.log("Usuario persistido");

      }
    }
  )
}

//identifico lo que voy a sacar fuera, que se identifica como indica a la derecha.
//sin esta linea no se podrá enlazar desde fuera
module.exports.writeUserDataToFile = writeUserDataToFile;
