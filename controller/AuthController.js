// Import external libraries
require('dotenv').config();
const io = require('../io')
const crypt = require('../crypt');
const requestJson = require('request-json');

//URL Base of Mongo db backend for V2
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuejv9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
function postLoginV1(req,res) {
    console.log("POST /apitechu/v1/login");
    console.log("parametro email: " + req.body.email);
    //Rememer never show passwords in console!!
    //console.log("parametro password: " + req.body.password);

    var users = require('../users.json');

    var logged = false;
    var userId = false;
    for (var i = 0; i < users.length; i++) {
      if (users[i].email == req.body.email) {
        console.log("User exists");
        if (users[i].password == req.body.password){
          logged = true;
          userId = users[i].id;
          console.log("Correct password");
          users[i].logged = true;
          io.writeUserDataToFile(users);
          console.log("Persisted file");
          break;
        } else {
          console.log("Password incorrect");
          break;
        }
      }
    }
    if (logged) {
      res.send('{ "mensaje" : "Login correcto", "idUsuario" : '+ userId + ' }')
    } else {
      res.send('{ "mensaje" : "Login incorrecto" }')
    }

}


function postLogoutV1(req,res) {
  console.log("POST /apitechu/v1/logout/:id");
  console.log("id to delete is " + req.params.id);

  var users = require('../users.json');

  var logout = false;
  var userId = req.params.id;

  for (var i = 0; i < users.length; i++) {
    if (users[i].id == userId) {
      console.log("User exists");
      if (users[i].logged){
        logout = true;
        console.log("User unlogged");
        delete users[i].logged;
        io.writeUserDataToFile(users);
        console.log("Persisted file");
        break;
      } else {
        console.log("User is not logged");
        break;
      }
    }
  }
  if (logout) {
    res.send('{ "mensaje" : "logout correcto", "idUsuario" : '+ userId + ' }')
  } else {
    res.send('{ "mensaje" : "logout incorrecto" }')
  }
}

function postLoginV2(req,res) {
    console.log("POST /apitechu/v2/login");
    console.log("parametro email: " + req.body.email);
    //Never show passwords in console!!
    //console.log("parametro password: " + req.body.password);
    var response = {"msg" : "Bad login"};
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Mongo httpClient created");

    httpClient.get('user?q={"email":"'+req.body.email+'"}&' + mLabAPIKey,
      function(err, resMLab, body){
        console.log(baseMlabURL + 'user?q={"email":"'+req.body.email+'"}&' + mLabAPIKey);
        if (err) {
          res.status(500);
          response = {"msg": "Error in database connection"};
        } else {
          if (body.length > 0){
            var userFoundInBD = body[0];
            //Lógica de login
            console.log("User found with id = " + userFoundInBD.id);
            //Comprobar la password
            if (crypt.checkPassword(req.body.password,userFoundInBD.password)) {
              var putBody = '{"$set":{"logged":true}}';
              response = {"msg" : "Login OK", "id" :  userFoundInBD.id};
              httpClient.put('user?q={"email":"'+req.body.email+'"}&' + mLabAPIKey, JSON.parse(putBody),
                function(err, resMLab, body){
                  console.log("PUT logged OK");
                });
            } else {
              res.status(401);
              //response = {"msg" : "Password incorrect"}
            }
          } else {
            res.status(401);
            //response = {"msg" : "User not found"}
        }
      }
      res.send(response);
    });
}


function postLogoutV2(req,res) {
  console.log("POST /apitechu/v2/logout/:id");
  console.log("id to logout is " + req.params.id);

  var response = {"msg" : "Bad request"};
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Mongo httpClient created");

  httpClient.get('user?q={"id":'+req.params.id+'}&' + mLabAPIKey,
    function(err, resMLab, body){
      console.log(baseMlabURL + 'user?q={"id":'+req.params.id+'}&' + mLabAPIKey);
      if (err) {
        res.status(500);
        response = {"msg": "Error in database connection"};
      } else {
        if (body.length > 0){
          var userFoundInBD = body[0];
          //Lógica de login
          console.log("User found with id = " + userFoundInBD.id);
          //Comprobar la password
          if (userFoundInBD.logged) {
            var putBody = '{"$unset":{"logged":""}}';
            response = {"msg" : "Logout OK with ID " +  userFoundInBD.id};
            httpClient.put('user?q={"id":'+userFoundInBD.id+'}&' + mLabAPIKey, JSON.parse(putBody),
              function(err, resMLab, body){
                console.log("PUT remove logged OK");
              });
          } else {
            res.status(400);
            //response = {"msg" : "USer not logged"}
          }
        } else {
          res.status(400);
          //response = {"msg" : "User not found"}
      }
    }
    res.send(response);
  });
}





module.exports.postLoginV1 = postLoginV1;
module.exports.postLogoutV1 = postLogoutV1;
module.exports.postLoginV2 = postLoginV2;
module.exports.postLogoutV2 = postLogoutV2;
