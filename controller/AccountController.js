// Import external libraries
require('dotenv').config();
const requestJson = require('request-json');

//URL Base of Mongo db backend for V2
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuejv9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountByUserV1(req,res) {
    console.log("GET /apitechu/v1/account/:id");

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Mongo httpClient created");

    var id = req.params.id;
    var query = 'q={"userid":' + id + "}";

    var url = "account?" + query + "&" + mLabAPIKey;
    console.log(baseMlabURL + url);
    var response = {"msg" : "Error"};
    httpClient.get(url,
      function(err, resMLab, body){
        if (err) {
          res.status(500);
          response = {"msg" : "Internal server error"};
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            res.status(404);
            response = {"msg" : "Account not found"};
          }
        }
        res.send(response);
      }
    )

}

//https://api.mlab.com/api/1/databases/apitechuejv9ed/collections/account?q={"userid":31}





module.exports.getAccountByUserV1 = getAccountByUserV1;
