//Declare external libraries
require('dotenv').config();
const crypt = require('../crypt');
const requestJson = require('request-json');

//URL Base of Mongo db backend
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuejv9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUserV2(req,res) {
    console.log("GET /apitechu/v2/user");

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Mongo httpClient created");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
          "msg": "Error while getting user list"
        }
        res.send(response);
      }
    )

}

function getUserByIdV2(req,res) {
    console.log("GET /apitechu/v2/user/:id");

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Mongo httpClient created");

    var id = req.params.id;
    var query = 'q={"id":' + id + "}";

    var url = "user?" + query + "&" + mLabAPIKey;
    console.log(baseMlabURL + url);
    httpClient.get(url,
      function(err, resMLab, body){
        if (err) {
          res.status(500);
          var response = {"msg" : "Internal server error getting user"}
        } else {
            if (body.length > 0){
              var response = body[0];
            } else {
              res.status(404);
              var response = {"msg" : "User not found"}
            }
        }
        res.send(response);
      }
    )

}

function postUserV2(req,res) {
    console.log("POST /apitechu/v2/user/");

    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Mongo httpClient created");

    //TODO: User Data verification
      //verify that email does not exists

    var url = 'user?q={"email":"'+req.body.email+'"}' + mLabAPIKey;
    /*console.log(baseMlabURL + url);
    httpClient.get(url,
      function(err, resMLab, body){
        if (err) {
          var response = {
            "msg" : "Internal server error getting user"
          }
          res.status(500);
        } else {
                res.status(201);
                var response =  {
                  "msg" : "User saved"
                }
              }
              res.send(response);
            }
          )*/

      //TODO: Verify password is good enough

    //TODO: get last id from database

    //insert user in database



    //TODO: validate user Parameters
    var newUser = {
      "id": req.body.id,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": crypt.hash(req.body.password)
    };




    var url = "user?" + mLabAPIKey;
    console.log(baseMlabURL + url);
    httpClient.post(url, newUser, function(err, resMLab, body){
        if (err) {
          var response = {
            "msg" : "Internal server error getting user"
          }
          res.status(500);
        } else {
          res.status(201);
          var response =  {
            "msg" : "User saved"
          }
        }
        res.send(response);
      }
    )




//    "id": 22,
//    "first_name": "Lowrance",
//    "last_name": "Salzberg",
//    "email": "lsalzbergl@senate.gov",
//    "password": "NfACw4W1xPJ3"

}

module.exports.getUserV2 = getUserV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.postUserV2 = postUserV2;
