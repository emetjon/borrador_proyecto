//USe of express module
// IMPORTANT!! JSON parser (if not body is undefined)
require('dotenv').config();
const express = require('express');
const app = express();

//Import libraries
const authController = require('./controller/AuthController');
const userControllerV2 = require('./controller/UserControllerV2');
const accountController = require('./controller/AccountController');

//Declare a constant with the listening port
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json());
app.use(enableCORS);

app.listen(port);
console.log("API listening in port " + port);


app.post('/apitechu/v1/login', authController.postLoginV1);
app.post('/apitechu/v1/logout/:id', authController.postLogoutV1);

app.post('/apitechu/v2/login', authController.postLoginV2);
app.post('/apitechu/v2/logout/:id', authController.postLogoutV2);

app.get('/apitechu/v2/user/', userControllerV2.getUserV2);

app.get('/apitechu/v2/user/:id', userControllerV2.getUserByIdV2);

app.post('/apitechu/v2/user/', userControllerV2.postUserV2);

app.get('/apitechu/v1/account/:id', accountController.getAccountByUserV1);


app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"Welcome to APITechU!"});
  }
)

app.get('/apitechu/v1/users',
  function(req, res){
    console.log("GET /apitechu/v1/users");

    //Parameter read
    var top = req.query.$top;
    console.log("Parameter top = " + top);
    var count = req.query.$count;
    console.log("Parameter count = " + count);
    console.log("users: Parameters read");

    //load user file
    var users = require('./users.json');

    //inicialice result variable
    var result = {};

    //Process count parameter
    if (count){
      console.log("Count = true");
      result.count = users.length;
    }

    //Process top parameter
    /*if (top){
      console.log("top = " +  top);
      users = users.slice(0,top);
    }*/

    result.users=top?users.slice(0,top):users;
    //var topReturn = users.slice(1,top);
    res.send(result);

    //res.send({"msg":"lista de usuarios"});
  }
)

app.post('/apitechu/v1/users',
  function(req,res) {
    console.log("POST /apitechu/v1/users");
    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser = {
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email
    };
    //cargamos los usuarios
    var users = require("./clients.json");
    //agregamos el nuevo usuario
    users.push(newUser);
    console.log("Usuario añadido");

    writeUserDataToFile(users);
/*
    //cargamos libreria acceso io
    const fs = require('fs');
    //transformo los datos a un formato que luego se pueda leer
    var jsonUserData = JSON.stringify(users);
    //guardo en un nuevo fichero la nueva lista con el nuevo usuario guardado
    fs.writeFile("./usuarios2.json", jsonUserData,"utf8",
      function(err){
        if(err){
          console.log(err);
        } else {
          console.log("Usuario persistido");

        }
      }
    )
    */
    res.send(users);
  }
)

//Vamos a hacer un handler para los delete. paso por parametro el id del usuario a borrar
app.delete('/apitechu/v1/users/:id',
 function(req,res) {
     console.log("DELETE /apitechu/v1/users/:id");

     console.log("id to delete is " + req.params.id);
      //cargo usuarios
      var users = require('./clients.json');
      var notDeleted = true;

      //OPTION 1
      /*for (var i = 0; i < users.length; i++) {
        if (users[i].id == req.params.id){
          users.splice(i, 1);
          console.log("Client deleted");
          writeUserDataToFile(users);
          res.send("200 OK");
          notDeleted = false;
          break;
        }
      }*/

      //OPTION 2
      /*users.forEach(function (item, index, object) {
        if (item.id == req.params.id){
          object.splice(index, 1);
          console.log("Client deleted");
          writeUserDataToFile(users);
          res.send("200 OK");
          notDeleted = false;
        }
      });*/

      //OPTION 3 for element in object
      //Not recommended for array. Better for
      /*for (index in users){
        if (users[index].id == req.params.id){
          users.splice(index, 1);
          console.log("Client deleted");
          writeUserDataToFile(users);
          res.send("200 OK");
          notDeleted = false;
        }
      }*/

      //OPTION 4 for element in object
      /*var index = 0;
      for (user of users){
        if (user.id == req.params.id){
          users.splice( index , 1);
          console.log("Client deleted");
          writeUserDataToFile(users);
          res.send("200 OK");
          notDeleted = false;
        }
        index++;
      }*/

      //OPTION 4 for element in object Destructurin nodeJS v6
      /*for (var [index, user] of users.entries()){
        if (user.id == req.params.id){
          users.splice( index , 1);
          console.log("Client deleted");
          writeUserDataToFile(users);
          res.send("200 OK");
          notDeleted = false;
        }
        index++;
      }*/

      //OPTION 5 for element in object
      /*users.forEach(function (user,index){
        if (user.id == req.params.id){
          users.splice(index, 1);
          console.log("Client deleted");
          writeUserDataToFile(users);
          res.send("200 OK");
          notDeleted = false;
        }
      })*/
      /* if (notDeleted){
        console.log("Client does not exist");
        res.send("204 Client does not exist");
      }*/

      // The most elegant
      var indexOfElement = users.findIndex(
        function (element){
          return element.id == req.params.id;
        }
      )
      if (indexOfElement >= 0){
        users.splice(indexOfElement, 1);
        console.log("Client deleted");
        writeUserDataToFile(users);
        res.send("200 OK");
      } else {
        console.log("Client does not exist");
        res.send("204 Client does not exist");
      }

 }
)


//creo una funcion que me escriba en disco una lista de usuarios
function writeUserDataToFile(data){
  console.log("writeUserDataToFile");

  //cargamos libreria acceso io
  const fs = require('fs');
  //transformo los datos a un formato que luego se pueda leer
  var jsonUserData = JSON.stringify(data);
  //guardo en un nuevo fichero la nueva lista con el nuevo usuario guardado
  fs.writeFile("./clients.json", jsonUserData,"utf8",
    function(err){
      if(err){
        console.log(err);
      } else {
        console.log("Usuario persistido");

      }
    }
  )
}
