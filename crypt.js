const bcrypt = require('bcrypt');

function hash(data) {
  console.log("CRYPT Hashing data");
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
  console.log("CRYPT Checking password");
  return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
